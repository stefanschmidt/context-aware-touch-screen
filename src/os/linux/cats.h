/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * SPDX-FileCopyrightText: Huawei Inc.
 */

#ifndef _CATS_H_
#define _CATS_H_

#include <lvgl/lvgl.h>
#include <stdio.h>

#define LOG_DBG(_str, ...) printf(_str "\n", ##__VA_ARGS__)
#define LOG_INFO(_str, ...) printf(_str "\n", ##__VA_ARGS__)
#define LOG_ERR(_str, ...) printf(_str "\n", ##__VA_ARGS__)

enum {
	CATS_SCREEN_HORIZONTAL,
	CATS_SCREEN_VERTICAL,
};

struct cats_screen {
	struct cats_screen *prev, *next;
	lv_obj_t *obj;
	const char *name;
	int orientation;
};

void cats_add_screen(struct cats_screen *scr);

#endif /* _CATS_H_ */
