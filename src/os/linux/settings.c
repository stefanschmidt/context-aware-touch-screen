/*
 * SPDX-License-Identifier: Apache-2.0

 * SPDX-FileCopyrightText: Huawei Inc.
 */

#include "cats.h"

static struct cats_screen scr;
static lv_obj_t *theme_list;

static void theme_change_handler(lv_event_t *event)
{
	bool dark = false;
	char buf[16];

	lv_dropdown_get_selected_str(theme_list, buf, sizeof(buf));

	LOG_DBG("Changing theme to: %s", buf);

	if (strcmp(buf, "Default") == 0)
		dark = false;
	else if (strcmp(buf, "Dark") == 0)
		dark = true;

	lv_theme_default_init(NULL,
			      lv_palette_main(LV_PALETTE_BLUE),
			      lv_palette_main(LV_PALETTE_RED),
			      dark,
			      &lv_font_montserrat_14);
}

void register_settings_screen()
{
	lv_obj_t *label, *cont;

	LOG_DBG("Creating settings menu screen");

	scr.name = "settings";
	scr.obj = lv_obj_create(NULL);
	scr.orientation = CATS_SCREEN_HORIZONTAL;

	label = lv_label_create(scr.obj);
	lv_label_set_text(label, "Settings");
	lv_obj_align(label, LV_ALIGN_TOP_MID, 0, 0);

	cont = lv_obj_create(scr.obj);
	lv_obj_align(cont, LV_ALIGN_CENTER, 0, 0);
        lv_obj_set_flex_flow(cont, LV_FLEX_FLOW_COLUMN);
	lv_obj_set_size(cont, 220, 280);

	theme_list = lv_dropdown_create(cont);
	lv_dropdown_set_options(theme_list, "Dark\nDefault");
	lv_obj_add_event_cb(theme_list, theme_change_handler,
			    LV_EVENT_VALUE_CHANGED, NULL);

	cats_add_screen(&scr);
}
